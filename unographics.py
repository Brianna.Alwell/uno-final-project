import pygame as pg
import random

SCREEN_WIDTH = 750
SCREEN_HEIGHT = 750

bg_color = 0
game_window = None #Reference to the pygame window
clock = pg.time.Clock()
quitflag = False
prev_time = 0


visible_sprites = pg.sprite.LayeredUpdates() #Holds layers of visible sprites


# background colors
green = (0, 255, 0)
blue = (0, 0, 255)
red = (255, 0, 0)
black = (0,0,0)
white = (255,255,255)
gray = (162,162,162)

# button images
back_button = pg.image.load('buttons/backbutton.png')
black_settings_button = pg.image.load('buttons/blackbutton.png')
blue_settings_button = pg.image.load('buttons/bluebutton.png')
red_settings_button = pg.image.load('buttons/redbutton.png')
green_settings_button = pg.image.load('buttons/greenbutton.png')
white_settings_button = pg.image.load('buttons/whitebutton.png')
new_game_button = pg.image.load('buttons/newgamebutton.png')
settings_button = pg.image.load('buttons/settingsbutton.png')
start_button = pg.image.load('buttons/startbutton.png')
quit_button = pg.image.load('buttons/quitbutton.png')
card_back_raw = pg.image.load('uno_card_back.png')
card_back = pg.transform.scale(card_back_raw,(80,112))

# red cards
red_cards0=pg.image.load('cards/redcard0.png')
red_card0=pg.transform.scale(red_cards0,(80,112))
red_card1=pg.image.load('cards/redcard1.png')
red_card1=pg.transform.scale(red_card1,(80,112))
red_card2=pg.image.load('cards/redcard2.png')
red_card2=pg.transform.scale(red_card2,(80,112))
red_card3=pg.image.load('cards/redcard3.png')
red_card3=pg.transform.scale(red_card3,(80,112))
red_card4=pg.image.load('cards/redcard4.png')
red_card4=pg.transform.scale(red_card4,(80,112))
red_card5=pg.image.load('cards/redcard5.png')
red_card5=pg.transform.scale(red_card5,(80,112))
red_card6=pg.image.load('cards/redcard6.png')
red_card6=pg.transform.scale(red_card6,(80,112))
red_card7=pg.image.load('cards/redcard7.png')
red_card7=pg.transform.scale(red_card7,(80,112))
red_card8=pg.image.load('cards/redcard8.png')
red_card8=pg.transform.scale(red_card8,(80,112))
red_card9=pg.image.load('cards/redcard9.png')
red_card9=pg.transform.scale(red_card9,(80,112))
red_card_plus=pg.image.load('cards/redCardPlus2.png')
red_card_plus=pg.transform.scale(red_card_plus,(80,112))
red_card_skip = pg.image.load('cards/redCardSkip.png')
red_card_skip = pg.transform.scale(red_card_skip,(80,112))
red_card_reverse = pg.image.load('cards/redCardReverse.png')
red_card_reverse = pg.transform.scale(red_card_reverse,(80,112))
# blue cards
blue_card0=pg.image.load('cards/bluecard0.png')
blue_card0=pg.transform.scale(blue_card0,(80,112))
blue_card1=pg.image.load('cards/bluecard1.png')
blue_card1=pg.transform.scale(blue_card1,(80,112))
blue_card2=pg.image.load('cards/bluecard2.png')
blue_card2=pg.transform.scale(blue_card2,(80,112))
blue_card3=pg.image.load('cards/bluecard3.png')
blue_card3=pg.transform.scale(blue_card3,(80,112))
blue_card4=pg.image.load('cards/bluecard4.png')
blue_card4=pg.transform.scale(blue_card4,(80,112))
blue_card5=pg.image.load('cards/bluecard5.png')
blue_card5=pg.transform.scale(blue_card5,(80,112))
blue_card6=pg.image.load('cards/bluecard6.png')
blue_card6=pg.transform.scale(blue_card6,(80,112))
blue_card7=pg.image.load('cards/bluecard7.png')
blue_card7=pg.transform.scale(blue_card7,(80,112))
blue_card8=pg.image.load('cards/bluecard8.png')
blue_card8=pg.transform.scale(blue_card8,(80,112))
blue_card9=pg.image.load('cards/bluecard9.png')
blue_card9=pg.transform.scale(blue_card9,(80,112))
blue_card_plus=pg.image.load('cards/blueCardPlus2.png')
blue_card_plus=pg.transform.scale(blue_card_plus,(80,112))
blue_card_skip = pg.image.load('cards/blueCardSkip.png')
blue_card_skip = pg.transform.scale(blue_card_skip,(80,112))
blue_card_reverse = pg.image.load('cards/blueCardReverse.png')
blue_card_reverse = pg.transform.scale(blue_card_reverse,(80,112))
# green cards
green_card0=pg.image.load('cards/greencard0.png')
green_card0=pg.transform.scale(green_card0,(80,112))
green_card1=pg.image.load('cards/greencard1.png')
green_card1=pg.transform.scale(green_card1,(80,112))
green_card2=pg.image.load('cards/greencard2.png')
green_card2=pg.transform.scale(green_card2,(80,112))
green_card3=pg.image.load('cards/greencard3.png')
green_card3=pg.transform.scale(green_card3,(80,112))
green_card4=pg.image.load('cards/greencard4.png')
green_card4=pg.transform.scale(green_card4,(80,112))
green_card5=pg.image.load('cards/greencard5.png')
green_card5=pg.transform.scale(green_card5,(80,112))
green_card6=pg.image.load('cards/greencard6.png')
green_card6=pg.transform.scale(green_card6,(80,112))
green_card7=pg.image.load('cards/greencard7.png')
green_card7=pg.transform.scale(green_card7,(80,112))
green_card8=pg.image.load('cards/greencard8.png')
green_card8=pg.transform.scale(green_card8,(80,112))
green_card9=pg.image.load('cards/greencard9.png')
green_card9=pg.transform.scale(green_card9,(80,112))
green_card_plus = pg.image.load('cards/greenCardPlus2.png')
green_card_plus = pg.transform.scale(green_card_plus,(80,112))
green_card_skip = pg.image.load('cards/greenCardSkip.png')
green_card_skip = pg.transform.scale(green_card_skip,(80,112))
green_card_reverse = pg.image.load('cards/greenCardReverse.png')
green_card_reverse = pg.transform.scale(green_card_reverse,(80,112))

# yellow cards
yellow_card0=pg.image.load('cards/yellowcard0.png')
yellow_card0=pg.transform.scale(yellow_card0,(80,112))
yellow_card1=pg.image.load('cards/yellowcard1.png')
yellow_card1=pg.transform.scale(yellow_card1,(80,112))
yellow_card2=pg.image.load('cards/yellowcard2.png')
yellow_card2=pg.transform.scale(yellow_card2,(80,112))
yellow_card3=pg.image.load('cards/yellowcard3.png')
yellow_card3=pg.transform.scale(yellow_card3,(80,112))
yellow_card4=pg.image.load('cards/yellowcard4.png')
yellow_card4=pg.transform.scale(yellow_card4,(80,112))
yellow_card5=pg.image.load('cards/yellowcard5.png')
yellow_card5=pg.transform.scale(yellow_card5,(80,112))
yellow_card6=pg.image.load('cards/yellowcard6.png')
yellow_card6=pg.transform.scale(yellow_card6,(80,112))
yellow_card7=pg.image.load('cards/yellowcard7.png')
yellow_card7=pg.transform.scale(yellow_card7,(80,112))
yellow_card8=pg.image.load('cards/yellowcard8.png')
yellow_card8=pg.transform.scale(yellow_card8,(80,112))
yellow_card9=pg.image.load('cards/yellowcard9.png')
yellow_card9=pg.transform.scale(yellow_card9,(80,112))
yellow_card8=pg.image.load('cards/yellowcard8.png')
yellow_card8=pg.transform.scale(yellow_card8,(80,112))
yellow_card9=pg.image.load('cards/yellowcard9.png')
yellow_card9=pg.transform.scale(yellow_card9,(80,112))
yellow_card_plus=pg.image.load('cards/yellowCardPlus2.png')
yellow_card_plus=pg.transform.scale(yellow_card_plus,(80,112))
yellow_card_skip = pg.image.load('cards/yellowCardSkip.png')
yellow_card_skip = pg.transform.scale(yellow_card_skip,(80,112))
yellow_card_reverse = pg.image.load('cards/yellowCardReverse.png')
yellow_card_reverse = pg.transform.scale(yellow_card_reverse,(80,112))
 
 # wild cards
wild_card=pg.image.load('cards/wildCard.png')
wild_card=pg.transform.scale(wild_card,(80,112))
wild_card_draw4=pg.image.load('cards/wildCardPlus4.png')
wild_card_draw4=pg.transform.scale(wild_card_draw4,(80,112))

# array list of cards


red_cards= [red_card0,red_card1,red_card2,red_card3,red_card4,red_card5,red_card6,red_card7,red_card8,red_card9,red_card_skip,red_card_reverse,red_card_plus]
blue_cards = [blue_card0,blue_card1,blue_card2,blue_card3,blue_card4,blue_card5,blue_card6,blue_card7,blue_card8,blue_card9,blue_card_skip,blue_card_reverse,blue_card_plus]
yellow_cards = [yellow_card0,yellow_card1,yellow_card2,yellow_card3,yellow_card4,yellow_card5,yellow_card6,yellow_card7,yellow_card8,yellow_card9,yellow_card_skip,yellow_card_reverse,yellow_card_plus]
green_cards = [green_card0,green_card1,green_card2,green_card3,green_card4,green_card5,green_card6,green_card7,green_card8,green_card9,green_card_skip,green_card_reverse,green_card_plus]
wild_cards= [wild_card,wild_card_draw4]

dt = 0 #frame time

def window_setup():
    global game_window
    pg.init()
    game_window = pg.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))

def update_logistics(): #Handles making it not crash, exiting, etc
    clock.tick(60)
    global quitflag
    global dt
    dt = delta_time_update()
    for e in pg.event.get():
        if e.type == pg.QUIT:
            quitflag = True
        elif e.type == pg.MOUSEBUTTONDOWN:
            
            for s in visible_sprites.get_sprites_at(mouse_pos()):
                if(s.click_handler != None):
                    s.click_handler(1)
        elif e.type == pg.MOUSEBUTTONUP:
            for s in visible_sprites.sprites():
                if(s.click_handler != None):
                    s.click_handler(0)


def update_screen():

    game_window.fill(bg_color)

    #draw only if group not empty
    if len(visible_sprites.sprites())>0:
        visible_sprites.draw(game_window)
    pg.display.flip()

def mouse_pos():
    return pg.mouse.get_pos()

def sprite_at(pos):
    return visible_sprites.get_sprites_at(pos)[0]

def delta_time_update():
    global prev_time
    curr = pg.time.get_ticks()
    out = curr-prev_time
    prev_time = curr
    return out
def get_delta_time():
    return dt
def import_image(path):
    return pg.image.load(path)

#Anything that gets drawn on screen should have one of these!

class GameSprite(pg.sprite.Sprite):
    def __init__(self, x, y, width, height, image, layer=0, click_handler = None):
        super().__init__()
        self._layer = layer #This controls which layer the sprite will be drawn on; Higher number = higher layer
        self.image = image
        self.image = pg.transform.scale(self.image, (width,height))
        self.rect = self.image.get_rect()
        self.click_handler = click_handler
        self.rect.x = x
        self.rect.y = y
        self.prev_time = 0
        self.delta_time = 0
        self.curr_time = 0

    def move(self, dx, dy): #Alters sprite position by dx, dy
        self.rect.x += dx
        self.rect.y += dy

    def set_pos(self, x, y): #Sets sprite position
        self.rect.x = x
        self.rect.y = y

    def is_touching(self, spr2):

        return pg.sprite.collide_rect(self,spr2) and self != spr2

    def get_pos(self,): #Gets sprite position
        return (self.rect.x,self.rect.y)

    def point_on_sprite(self, pos): #Returns true if the given (x,y) tuple is inside of the sprite's area
        return self.rect.collidepoint(pos)

    def area_on_sprite(self, x, y, w, h): #Returns true if there is overlap between the given rectangle and this sprite
        return self.rect.colliderect(pg.Rect(x, y, w, h))

    def rect_on_sprite(self, rec): #See above, just takes a Rect object instead of params.
        return self.rect.colliderect(rec)

    def get_layer(self): #Returns the layer of the current sprite
        return visible_sprites.get_layer_of_sprite(self)

    def other_at_loc(self,):
        for s in visible_sprites:
            if(s!=self and pg.sprite.collide_rect(s,self)):
                return s
        return None

    def change_render_layer(self, num): #Change the layer of the current sprite in visible_sprites
        visible_sprites.change_layer(self, num)

    def draw(self,): #Draw this sprite to the screen. 
        game_window.blit(self.image, self.rect)

    def render(spr): #Adds the sprite passed to the renderer.
        if not spr in visible_sprites:
            visible_sprites.add(spr)
    
    def unrender(spr): #Removes the passed sprite from the renderer.
        #if spr in visible_sprites:
        visible_sprites.remove(spr)

    def is_clicked(self): #Returns true if a sprite is clicked by mouse
        return pg.mouse.get_pressed()[0] and self.rect.collidepoint(pg.mouse.get_pos())

def quit():
    pg.display.quit()
    pg.quit()
        
    
    

