# Game and Player classes
NUMCARDS = 7
deck = ''
pile = ''
prev_player = ''
card_debt = 0

#from selectors import EpollSelector

import random
from deck import Deck
from deck import Color
from deck import Card
import pygame as pg

import unographics as ug
from time import process_time
from enum import Enum

from random import randrange
import random

st_Image = ug.import_image("buttons/startbutton.png")
bk_Image = ug.import_image("buttons/backbutton.png")
qt_Image = ug.import_image("buttons/quitbutton.png")
uno_Image = ug.import_image("uno_button.png")
logo_Image = ug.import_image("logo_white.png")
new_Image = ug.import_image("buttons/newgamebutton.png")
menu_Image = ug.import_image("buttons/settingsbutton.png")
exit_Image = ug.import_image("buttons/quitbutton.png")
uno_button_image = ug.import_image("uno_button.png")
settings_image = ug.import_image("buttons/settingsbutton.png")


green_settings_button = ug.import_image("buttons/greenbutton.png")
blue_settings_button = ug.import_image("buttons/bluebutton.png")
red_settings_button = ug.import_image("buttons/redbutton.png")
white_settings_button = ug.import_image("buttons/whitebutton.png")
black_settings_button = ug.import_image("buttons/blackbutton.png")

class Game:

    uno = False

    def __init__(self,):
        # Initialize Deck
        self.deck = Deck(is_hidden=True, handler_index=0)
        self.deck.spr.render()
        self.reverse = False
        
        # Create Players
        self.player_one = Player(self.deck, name = "Player", pos=0)
        self.player_two = Player(self.deck, bot = True, name = "Bot 1", pos = 1)
        self.player_three = Player(self.deck, bot = True, name = "Bot 2", pos = 2)
        self.player_four = Player(self.deck, bot = True, name = "Bot 3", pos = 3)

        # Array of players
        self.player_array = []
        self.player_array = self.player_array + [self.player_one, self.player_two, self.player_three, self.player_four]
        self.current_player = 0

        # Store currently held card
        self.dragged_card = None

    def get_player(self,):
        return self.player_array[self.current_player]

    def advance_turn(self, is_skip = False, is_reverse = False):
        global prev_player
        prev_player = self.get_player()
        if(is_skip):
            self.current_player += 1
        if(is_reverse):
            self.player_value = self.get_player()
            self.player_array = list(reversed(self.player_array))
            self.current_player = self.player_array.index(self.player_value)
        
        
        self.current_player = (self.current_player + 1) % len(self.player_array)
        return 0

    # check_uno checks if a player has one card and draws cards if button is pressed
    def check_uno(curr_player, deck):
        # if the current player is the USER
        button = False
        if (Player.get_name(curr_player) == "Player"):
        #     # if the array's length is equal to 1
            if len(curr_player.hand) == 2:
                # display uno button for 3 seconds
                button = Game.display_button()
                # if button is not pressed
                if (not button):
                    # user gets two cards added to their hand
                    curr_player.draw_card(deck)
                    curr_player.draw_card(deck)
        # if current player is CPU
        else:
            button = random.choice([True, False])
            # randomly return true or false
            if (not button):
                    # cpu gets two cards added to their hand
                    curr_player.draw_card(deck)
                    curr_player.draw_card(deck)


    # display_button dislays button on the screen for 3 seconds and returns True or False if pressed or not
    def display_button():
        
        class Uno_Button():
            def click_button(self, n):
                if(n==1):
                    self.spr.unrender()
                    
                    self.ret_flag = True
            def __init__(self,):
                self.ret_flag = False
                self.spr = ug.GameSprite(50,50,120,54,uno_Image, click_handler=self.click_button) # sample back button
                self.spr.render()

        button = Uno_Button()
        button.spr.set_pos(50, 550)

        start = process_time()
        end = process_time()

        ug.update_logistics()
        ug.update_screen()

        while(not button or end - start < 1):

            ug.update_logistics()
            ug.update_screen()

            if button.spr.is_clicked():
                return True

            end = process_time()
            

        button.spr.unrender()
        return False
    
    def __str__(self):
        out = ""
        out += "State of Game:\n\n Hands: "
        for p in self.player_array:
            out = out + "Name: " + p.name + ", Cards: "
            for c in p.hand:
                out += str(c.color) + " " + str(c.number) + ", \n"
        out += "\n\nIt is " + self.player_array[self.current_player].name + "'s Turn.\n"
        if(pile.top_card() != None):
            out += "The top card of the pile is " + str(pile.top_card().color) + " " + str(pile.top_card().number) + "\n"
        else:
            out += "There are no cards on the pile."
        return out

    # Graphics!
    def graphics_based_game():

        ug.window_setup()

        quit = False

        while not ug.quitflag:
            Game.display_button()
            ug.update_logistics()
            ug.update_screen()
        
        
        ug.quit()

# player class
class Player:

    # draw cards using the draw single card draw_card() to get a single card
    def draw_hand(self, deck, bot=False):
        hand = []
        if(bot):
            for x in range(0, NUMCARDS):
                crd = deck.draw_card()
                crd.flip()
                hand.append(crd)
        else:
            for x in range(0, NUMCARDS):
                hand.append(deck.draw_card())
        return hand

    def __init__(self, deck, bot = False, name = "noName", pos=0):
        self.name = name #Added a name attribute to make display easier
        if(bot):
            self.hand = self.draw_hand(deck, bot=True)
        else:
            self.hand = self.draw_hand(deck)
        self.turn = False
        self.bot = bot
        self.pos = pos

    # getters
    def get_turn(self):
        return self.turn

    def get_name(self):
        return self.name
    def is_winner(self):
        #print(len(self.hand)<1)
        return len(self.hand)<1
    # setters
    def set_turn(self, turn):
        self.turn = turn

    # game logic
    def play_card(self, pile, played, color = None):
        global card_debt
        if(played.number == 12):
            card_debt+=2
        if(color != None):
            played.color = color
        cardPlayed = False
        topCard = pile.top_card()
        # check if card that is wanted to be played is valid
        if pile.is_playable(played):
            # add card to the pile
            pile.add_card(played)
            self.hand.remove(played)
            cardPlayed = True
            played.spr.unrender()
            
        else:
            
            
            cardPlayed = False
        return cardPlayed
        # play card if is if not reask and suggest to draw from deck

    def plus2_give(self, num_plus2, deck):
        global prev_player
        

        for i in range(num_plus2*2):
            prev_player.draw_card(deck)


    def draw_card(self, deck):
        # add top card to hand return hand
        global pile
        reshuffle(deck,pile)
        if(self.bot):
            crd = deck.draw_card()
            crd.flip()
            self.hand.append(crd)
        else: 
            self.hand.append(deck.draw_card())
    
    def draw_till_playable(self, deck):
        
        drawn = 0
        playable = False
        while playable == False:
            # draw card 
            self.draw_card(deck)
            drawn += 1
            # check if playable
            drawnCard = self.hand[-1]
            #print(drawnCard)
            #rint("You drew " + str(drawnCard.color) + str(drawnCard.number))
            if pile.is_playable(drawnCard):
                # if playable prompt user for option to play
                #print("Your card has been played and your turn will be ended")
                drawnCard.flip()
                played = self.play_card(pile, drawnCard)
                # set playable to true to end loop
                if played == True:
                    
                    playable = True
            elif drawnCard.number == 12: # this will be 13 when stuff is fixed properly
                drawnCard.flip()
                played = self.play_card(pile, drawnCard)
                if played == True:
                    
                    playable = True

        return playable

    # player and bot logic
    def player_logic(self, pile, deck):
        
        playable = True
        unplayable = False
        topCard = pile.top_card()
        # check if the player is a bot or actual human
        if self.bot == True:
                # x = current card in the hand that is being looked at
                for x in self.hand:
                    # check if card is reverse skip or wild
                    if x.number == 10:
                        skip = x
                    if x.number == 11:
                        reverse = x
                    if x.number == 12:
                        wild = x
                    if x.color == topCard.color and x.number <= 9:
                        # play the card and remove it from hand
                        played = self.play_card(pile, x)
                        if played == True:
                            unplayable = False
                            break
                        # if play_card returns true then move on if not reset and try again
                    elif x.number == topCard.number:
                        # play the card and remove it from hand
                        played = self.play_card(pile, x)
                        if played == True:
                            unplayable = False
                            break

                # if a reverse, skip, or wild card exists play them in that order
                if unplayable == True:
                    # check skip
                    if skip != None:
                        # check that skip is the right color
                        if skip.color == topCard.color and unplayable == True:
                            played = self.play_card(pile, skip)
                            unplayable = False
                    # check reverse
                    if reverse != None:
                        if reverse.color == topCard.color and unplayable == True:
                            played = self.play_card(pile, reverse)
                            unplayable = False
                    # check wild
                    if wild != None and unplayable == True:
                        color = randrange(4)
                        played = self.play_card(pile, wild, Color(color+1))
                        unplayable = False
                        

                if unplayable == True: # if no card in hand is playable
                    # draw cards from deck until one is playable
                    
                    played = self.draw_till_playable(deck)
                    if played == True:
                        
                        unplayable = False
                    else:
                        print()
        elif self.bot == False:
            toppile = pile.top_card()
            cardInHand = False
            
            successful_play = False
            while not successful_play:
                
                x = input()
                if x == "exit":
                    return -1
                x = x.split(' ')
                if x[0] != "draw":
                    card = None
                    for i in self.hand:
                        # finding the card selected in hand
                        if i.color == Color[x[0]] and str(i.number) == x[-1]:
                            # card is found check if playable
                            card = i
                            break
                        
                    if card is None:
                        print("No matching card in hand.")
                        continue
                    
                    played = self.play_card(pile, card)
                    if played == True:
                        print("you have played your card successfully")
                        successful_play = True
                    else:
                        print("you have failed to play your card")
                        continue
                else:
                    draw = "yes"
                    while draw == "yes":
                        print("You now have chossen to draw your card")
                        played = self.draw_till_playable(deck)
                        if played == True:
                            print("Successfully drew till playable card was found and played")
                            draw = "no"
                            successful_play = True
                        else:
                            print("played is broken needs bug fixing")
                        
        return 1  

    def player_logic_graphics(self, pile, deck):
        global num_plustwos
        unplayable = True
        topCard = pile.top_card()
        if topCard == None or topCard.number != 12:
            num_plustwos = 0
        topisplus2 = False
        if topCard != None and topCard.number == 12:
            # if the top card is a plus 2 then add 2 to the previous player if the current player does not play a plus 2
            # check all cards before the current plus 2 until a card that is not a plus 2 is found
            num_plustwos += 1
        
        #if num_plustwos > 0:
            #print(str(num_plustwos) + " plus 2's in a row")


        # if the top card is a plus 2 the bot will prioritize playing a plus 2


        # check if the player is a bot or actual human
        if self.bot == True:
                skip = None
                reverse = None
                wild = None 
                plustwo = None
                playable = None
                # x = current card in the hand that is being looked at
                for x in self.hand:
                    # check if card is reverse skip or wild
                    if x.number == 10:
                        skip = x
                    if x.number == 11:
                        reverse = x
                    if x.number == 12:
                        plustwo = x
                    if x.number == 13:
                        wild = x
                    if x.color == topCard.color and x.number <= 9:
                        # play the card and remove it from hand
                        if topisplus2 == False:
                            x.flip()
                            played = self.play_card(pile, x)
                            if played == True:
                                unplayable = False
                                break
                        else:
                            # if a card that is skipped bc a plus 2 is on the top of the deck it should save that card and play it after checking the whole hand for a +2 (same color)
                            playable = x

                        # if play_card returns true then move on if not reset and try again
                    elif x.number == topCard.number:
                        # play the card and remove it from hand
                        if topisplus2 == False:    
                            x.flip()
                            played = self.play_card(pile, x)
                            if played == True:
                                unplayable = False
                                break
                        else:
                            playable = x
                    
                    elif x.number == 12:
                        # if a +2 is found it will play it
                        x.flip()
                        played = self.play_card(pile, x)
                        if played == True:
                            unplayable = False
                            break
                        
                # at the end of the loop if a card that is skipped over bc its looking for a plus 2 is skipped it should play that card
                if topisplus2 == True and playable != None:
                    # if this is done the previous player will be given the number of cards for all the +2s played in a row
                    playable.flip()
                    played = self.play_card(pile, playable)
                    
                    if played == True:
                        #self.plus2_give(num_plustwos, deck)
                        unplayable = False
                
                # check skip
                if skip != None and unplayable == True:
                    # check that skip is the right color
                    if skip.color == topCard.color:
                        skip.flip()
                        played = self.play_card(pile, skip)
                        unplayable = False

                # check reverse
                if reverse != None and unplayable == True:
                    if reverse.color == topCard.color:
                        reverse.flip()
                        played = self.play_card(pile, reverse)
                        unplayable = False
                
                # plus two
                if plustwo != None and unplayable == True:
                    if plustwo.color == topCard.color:
                        plustwo.flip()
                        played = self.play_card(pile, plustwo)
                        unplayable = False

                # check wild
                if wild != None and unplayable == True:
                    color = randrange(4)
                    wild.flip()
                    played = self.play_card(pile, wild, color = Color(color+1))
                    unplayable = False
                        

                if unplayable == True: # if no card in hand is playable
                    # draw cards from deck until one is playable
                   
                    played = self.draw_till_playable(deck)
                    if played == True:
                        
                        unplayable = False
                    else:
                        print()
                return True    
        elif self.bot == False:
            if(pile.has_update()): #True iff player has played a card
                return True
            return False
                        
        return 1        
class Dropzone:
    def drop_handler(self, n):
        if(n==0 and self.spr.point_on_sprite(ug.mouse_pos())):
            self.dropped_card = self.spr.other_at_loc()
            
    def get_dropped_card(self):
        return self.dropped_card
    def __init__(self, x, y, w, h):
        self.dropped_card = None
        self.spr=ug.GameSprite(x,y,w,h,None)

# if the deck runs out of cards, reshuffle all cards out of the playback into the deck
def reshuffle(deck, pile):
    # if the deck runs out of cards
    if deck.is_empty():
        # reshuffle all cards out of the pile into the deck
        for x in pile.cards:
            deck.add_card(x)
        # reset the pile
        random.shuffle(deck.cards)
        holdover = pile.top_card()
        pile.reset()
        pile.cards.append(holdover)
        
def text_based_game():
    global pile
    print("Welcome to text-based Uno! This is a pre-release intended for debugging, and does not include many game functions.")
    print("Currently supported is the ability for players and bots to take turns and draw/play cards until the player choses")
    print("To exit the game. Future pre-releases will include graphics and a feature-complete Uno experience.")
    print("-- Team 6 Dev Team\n\n\n")
    
    game = Game() #create the game object
    
    #Deal out a hand to each player/bot
    for p in game.player_array:
        p.draw_hand(game.deck)
    pile = Deck(is_standard = False)
    
    while(True):
        skip = False
        rev = False
        print(game)
        curr_player = game.player_array[game.current_player]
        quitted = curr_player.player_logic(pile, game.deck)
        if(quitted == -1):
            break

        # checking if player's hand is now one card
        # Game.check_uno(curr_player, game.deck)
        
        #Check for special card functions affecting turn order
        if(pile.top_card().number == 10):
            skip = True
        if(pile.top_card().number == 11):
            rev = True
            
        game.advance_turn(is_skip = skip, is_reverse = rev)
        
# text_based_game()

# Menus!

# Screen Enum
class Screen(Enum):
    # TITLE = 0 dont need anymore
    MAIN = 0
    START = 1
    SETTINGS = 2
    GAME = 3
    END = 4



# Loops through each screen
def menu_loop():
    
    window = ug.window_setup()
    quit = False
    screen = Screen.START

    while not ug.quitflag:
        if screen == Screen.START:
            screen = start_screen()
        if screen == Screen.GAME:
            screen = game_screen()
        if screen == Screen.MAIN:
            screen = main_screen()
        if screen == Screen.SETTINGS:
            screen = setting_screen()
        if screen == Screen.GAME:
            screen = game_screen()
        if screen == Screen.END:
            screen = end_screen()
            

    
    
    ug.quit()

def setting_screen():
    ug.window_setup()
    quit = False

    # settings stuff
    class BG_Color_Button():
        def __init__(self, button):
            self.spr = ug.GameSprite(50,50,144,54, button) # get a new button graphic for the setting button
            self.spr.render()

    green_button = BG_Color_Button(green_settings_button)
    green_button.spr.set_pos(25, 100)

    blue_button = BG_Color_Button(blue_settings_button)
    blue_button.spr.set_pos(200, 100)

    red_button = BG_Color_Button(red_settings_button)
    red_button.spr.set_pos(375, 100)

    white_button = BG_Color_Button(white_settings_button)
    white_button.spr.set_pos(25, 200)

    black_button = BG_Color_Button(black_settings_button)
    black_button.spr.set_pos(200, 200)

    # back to the menu button
    class BackButton():
        def __init__(self,):
            self.spr = ug.GameSprite(50,50,144,54, ug.import_image("buttons/backbutton.png")) # sample back button
            self.spr.render()

    back_button = BackButton()
    back_button.spr.set_pos(50, 675)


    while not ug.quitflag:
        if green_button.spr.is_clicked():
            # change bg_color to green
            ug.bg_color = ug.green

        if blue_button.spr.is_clicked():
            # change bg_color to green
            ug.bg_color = ug.blue

        if red_button.spr.is_clicked():
            # change bg_color to green
            ug.bg_color = ug.red

        if white_button.spr.is_clicked():
            ug.bg_color = ug.white
        
        if black_button.spr.is_clicked():
            ug.bg_color = ug.black

        if back_button.spr.is_clicked():
            back_button.spr.unrender()
            red_button.spr.unrender()
            blue_button.spr.unrender()
            green_button.spr.unrender()
            white_button.spr.unrender()
            black_button.spr.unrender()
            
            return Screen.START
        ug.update_logistics()
        ug.update_screen()



    
def start_screen():

    quit = False

    class StartButton():
        def click_button(self, n):
            if(n==1):
                self.spr.unrender()
                
                self.ret_flag = True
        def __init__(self,):
            self.ret_flag = False
            self.spr = ug.GameSprite(50,50,144,54,st_Image,click_handler=self.click_button) # sample start button
            self.spr.render()

    start_button = StartButton()


    start_button.spr.set_pos(300, 500)

    #settings_button = StartButton()
    #settings_button.spr.set_pos(330, 405)

    
    logo = ug.GameSprite(50, 50, 474, 377, logo_Image)
    logo.render()
    logo.set_pos(150,50)

    while not ug.quitflag:
        if(start_button.ret_flag):
            logo.unrender()
            return Screen.MAIN


        ug.update_logistics()
        ug.update_screen()
    
    
    ug.quit()

# Graphics!
def main_screen():
    quit = False

    class Button():
        def click_button(self, n):
            if(n==1):
                self.spr.unrender()
                
                self.ret_flag = True
        def __init__(self,):
            self.ret_flag = False
            self.spr = ug.GameSprite(50,50,144,54,bk_Image, click_handler=self.click_button) # sample back button
            self.spr.render()
    class Button2():
        def click_button(self, n):
            if(n==1 and self.spr.point_on_sprite(ug.mouse_pos())):
                self.spr.unrender()
                
                self.ret_flag = True
        def __init__(self,):
            self.ret_flag = False
            self.spr = ug.GameSprite(50,50,144,54,new_Image, click_handler=self.click_button)
            self.spr.render()
    class Button3():
        def click_button(self, n):
            if(n==1 and self.spr.point_on_sprite(ug.mouse_pos())):
                self.spr.unrender()
                
                self.ret_flag = True
        def __init__(self,):
            self.ret_flag = False
            self.spr = ug.GameSprite(50,50,144,54,exit_Image, click_handler=self.click_button)
            self.spr.render()
    class Button4():
        def click_button(self, n):
            if(n==1 and self.spr.point_on_sprite(ug.mouse_pos())):
                self.spr.unrender()
                
                self.ret_flag = True
        def __init__(self,):
            self.ret_flag = False
            self.spr = ug.GameSprite(50,50,144,54,settings_image, click_handler=self.click_button)
            self.spr.render()

    button1 = Button()
    button1.spr.set_pos(300, 500)

    button2 = Button2()
    button2.spr.set_pos(300, 550)

    button3 = Button3()
    button3.spr.set_pos(300, 600)

    button4 = Button4()
    button4.spr.set_pos(300, 660)

    while not ug.quitflag:
        if(button1.ret_flag):
            button2.spr.unrender()
            button3.spr.unrender()
            button4.spr.unrender()
            return Screen.START
        if(button2.ret_flag):
            button1.spr.unrender()
            button3.spr.unrender()
            button4.spr.unrender()
            return Screen.GAME
        if(button3.ret_flag):
            button2.spr.unrender()
            button1.spr.unrender()
            button4.spr.unrender()
            ug.quitflag = True
        if(button4.ret_flag):
            button2.spr.unrender()
            button3.spr.unrender()
            button1.spr.unrender()
            return Screen.SETTINGS

        ug.update_logistics()
        ug.update_screen()
    
    
    ug.quit()

def game_screen():
    ug.window_setup()
    delay_time = 0 #Time to delay in ms
    uno_time = 0
    uno_flag = False
    CEN = 750/2
    ICD_MAX = 90
    USABLE_WIDTH = 630
    dragging = False
    class QuitButton():
        def click_button(self, n):
            if(n==1):
                self.spr.unrender()
                self.ret_flag = True
        def __init__(self,):
            self.ret_flag = False
            self.spr = ug.GameSprite(50,50,144,54,bk_Image, click_handler=self.click_button) # sample back button
            self.spr.render()
    class UnoButton():
        def click_button(self, n):
            if(n==1):
                self.spr.unrender()
                self.ret_flag = True
        def __init__(self,):
            self.ret_flag = False
            self.spr = ug.GameSprite(150,150,100,100,uno_button_image, click_handler=self.click_button) # sample back button

    global pile
    game = Game()

    global card_debt
    for p in game.player_array:
        p.draw_hand(game.deck)
    pile = Deck(is_standard = False, handler_index=1)
    pile.spr.set_pos((750//2)-40,(750//2)-56)
    pile.spr.render()

    quit_button = QuitButton()
    uno_button = UnoButton()
    turn_done = False
    new_turn_flag = False
    uno_success_flag = False
    while not ug.quitflag:
        ug.update_logistics()
        ug.update_screen()
        
       
            
        if(uno_time > 0):
            uno_time -= ug.get_delta_time()
        if(delay_time<1):
            if(quit_button.ret_flag):
                ug.visible_sprites.empty()
                return Screen.START

            skip = False
            rev = False

            #Update hands of all players on screen

            for curr_player in game.player_array:
                if(curr_player.is_winner()):
                    ug.visible_sprites.empty()
                    return Screen.END
                if(curr_player.bot == True):
                    n = len(curr_player.hand)
                    
                    icd = (USABLE_WIDTH/3)/n
                    if n % 2 == 0:
                        leftmost = CEN-(icd*((n//2)-.5))
                    else:
                        leftmost = CEN-(icd*(n//2))
                    i = 0
                    if(curr_player.pos == 2): #Top bot
                        for c in curr_player.hand:
                            c.spr.render()
                            c.spr.set_pos(leftmost+(icd*i)-c.spr.rect.width/2, 38)
                            c.spr.change_render_layer(i+1)
                            i+=1
                    if(curr_player.pos == 1):
                        for c in curr_player.hand:
                            c.spr.render()
                            c.spr.set_pos(35, leftmost+(icd*i)-c.spr.rect.width/2)
                            c.spr.change_render_layer(i+1)
                            i+=1
                    if(curr_player.pos == 3):
                        for c in curr_player.hand:
                            c.spr.render()
                            c.spr.set_pos(750-(35+80), leftmost+(icd*i)-c.spr.rect.width/2)
                            c.spr.change_render_layer(i+1)
                            i+=1
                    
            curr_player = game.player_array[game.current_player]
            
            if(new_turn_flag):
                new_turn_flag=False
                if(card_debt!=0):
                    for i in range(card_debt):
                        curr_player.draw_card(game.deck)
                    card_debt=0
            
            #Ensure all hand cards/placeholders are rendered and in their proper positions/layers
            if(curr_player.bot == False):

                #Handle uno
                if(len(curr_player.hand)==1 and uno_flag==False): #Uno!
                    uno_flag = True
                    uno_time = 3000
                    uno_button.spr.render()
                elif(uno_flag and uno_time<1 and not uno_button.ret_flag and not uno_success_flag):#Failed uno
                    uno_flag = False
                    uno_time = 0
                    curr_player.draw_card(game.deck)
                    curr_player.draw_card(game.deck)
                    uno_button.spr.unrender()
                elif(uno_flag and uno_button.ret_flag): #win uno
                    uno_button.ret_flag = False
                    uno_time=0
                    uno_success_flag = True
                    


                
                if(curr_player.is_winner()):
                    return Screen.END
                if(dragging):
                    n = len(curr_player.hand)-1
                else:
                    n = len(curr_player.hand)            
                if(n<7):
                    icd = ICD_MAX
                else:
                    icd = USABLE_WIDTH/n

                if n % 2 == 0:
                    leftmost = CEN-(icd*((n//2)-.5))
                else:
                    leftmost = CEN-(icd*(n//2))

                i = 0
                dragging = False
                for c in curr_player.hand:
                    c.spr.render()

                    if(pile.play_flag and pile.card_sprite_buffer == c.spr): #A play-card event was triggered by the user on this card! Play the card!
                        pile.play_flag = False
                        curr_player.play_card(pile,c)
                        break #Skip the rest of the functions for this card.
                    
                    if(c.drag_flag):
                        dragging = True
                        c.spr.change_render_layer(100) #Arbitrarily high layer num
                        c.spr.set_pos(ug.mouse_pos()[0] - c.spr.rect.width/2 ,ug.mouse_pos()[1] - c.spr.rect.height/2)
                    else:
                        c.spr.set_pos(leftmost+(icd*i)-c.spr.rect.width/2, 600)
                        c.spr.change_render_layer(i+1)
                        i+=1
                if(game.deck.draw_flag):
                    game.deck.draw_flag = False
                    curr_player.draw_card(game.deck)
            else: #Bot case
                if(curr_player.is_winner()):
                    return Screen.END
                if(game.deck.draw_flag):
                    game.deck.draw_flag = False
                n = len(curr_player.hand)
                
                icd = (USABLE_WIDTH/3)/n
                if n % 2 == 0:
                    leftmost = CEN-(icd*((n//2)-.5))
                else:
                    leftmost = CEN-(icd*(n//2))
                i = 0
                if(curr_player.pos == 2): #Top bot
                    for c in curr_player.hand:
                        c.spr.render()
                        c.spr.set_pos(leftmost+(icd*i)-c.spr.rect.width/2, 38)
                        c.spr.change_render_layer(i+1)
                        i+=1
                if(curr_player.pos == 1):
                    for c in curr_player.hand:
                        c.spr.render()
                        c.spr.set_pos(35, leftmost+(icd*i)-c.spr.rect.width/2)
                        c.spr.change_render_layer(i+1)
                        i+=1
                if(curr_player.pos == 3):
                    for c in curr_player.hand:
                        c.spr.render()
                        c.spr.set_pos(750-(35+80), leftmost+(icd*i)-c.spr.rect.width/2)
                        c.spr.change_render_layer(i+1)
                        i+=1

                    

            # checking if player's hand is now one card
            #Game.check_uno(curr_player, game.deck)
            
            
            
            turn_done = curr_player.player_logic_graphics(pile, game.deck)
        

            if(curr_player.bot):
                if(curr_player.is_winner()):
                    return Screen.END
                n = len(curr_player.hand)
                icd = (USABLE_WIDTH/3)/n
                if n % 2 == 0:
                    leftmost = CEN-(icd*((n//2)-.5))
                else:
                    leftmost = CEN-(icd*(n//2))
                i = 0
                if(curr_player.pos == 2): #Top bot
                    for c in curr_player.hand:
                        c.spr.render()
                        c.spr.set_pos(leftmost+(icd*i)-c.spr.rect.width/2, 38)
                        c.spr.change_render_layer(i+1)
                        i+=1
            if(turn_done): #The user has played a card or the bot has ended their turn.
                #Check for special card functions affecting turn order
                if(not curr_player.bot):
                    uno_flag = False
                    uno_success_flag = False
                if(pile.top_card().number == 10):
                    skip = True
                if(pile.top_card().number == 11):
                    rev = True
                turn_done = False
                pile.has_update() #Clear pile update status
                game.advance_turn(is_skip = skip, is_reverse = rev)
                new_turn_flag = True
                delay_time = 500
        else:
            delay_time -= ug.get_delta_time()
            

        


    
def end_screen():
    # new game 
    # main menu
    # exit
    quit = False
    #new game button
    class NewGameButton():
        def click_button(self, n):
            if(n==1 and self.spr.point_on_sprite(ug.mouse_pos())):
                self.spr.unrender()
               
                self.ret_flag = True
        def __init__(self,):
            self.ret_flag = False
            self.spr = ug.GameSprite(50,50,144,54,new_Image, click_handler=self.click_button)
            self.spr.render()
    class ExitButton():
        def click_button(self, n):
            if(n==1 and self.spr.point_on_sprite(ug.mouse_pos())):
                self.spr.unrender()
                
                self.ret_flag = True
        def __init__(self,):
            self.ret_flag = False
            self.spr = ug.GameSprite(50,50,144,54,exit_Image, click_handler=self.click_button)
            self.spr.render()
    class MenuButton():
        def click_button(self, n):
            if(n==1 and self.spr.point_on_sprite(ug.mouse_pos())):
                self.spr.unrender()
                
                self.ret_flag = True
        def __init__(self,):
            self.ret_flag = False
            self.spr = ug.GameSprite(50,50,144,54,menu_Image, click_handler=self.click_button)
            self.spr.render()

    
    new_game_button = NewGameButton()
    new_game_button.spr.set_pos(300, 500)
    exit_game_button = ExitButton()
    exit_game_button.spr.set_pos(200, 550)
    menu_button = MenuButton()
    menu_button.spr.set_pos(400, 550)

    while not ug.quitflag:
        if(new_game_button.ret_flag):
            new_game_button.spr.unrender()
            exit_game_button.spr.unrender()
            menu_button.spr.unrender()
            return Screen.START
        if(exit_game_button.ret_flag):
            #???? should quit game?
            new_game_button.spr.unrender()
            exit_game_button.spr.unrender()
            menu_button.spr.unrender()
            ug.quitflag=True
            return Screen.END
        
        if(menu_button.ret_flag):
            new_game_button.spr.unrender()
            exit_game_button.spr.unrender()
            menu_button.spr.unrender()
            return Screen.MAIN

        ug.update_logistics()
        ug.update_screen()
    
    ug.quit()

       



    


    


#setting_screen()
menu_loop()
#game_screen()
