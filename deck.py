from collections import deque
import os.path
import enum
import random
import unographics as ug

class Color(enum.Enum):
    RED = 1
    GREEN = 2
    BLUE = 3
    YELLOW = 4
    
class Card():
    # card numbers
    #skip = 10
    #reverse = 11
    #draw2 = 12

    def flip(self): #Show the other face of the card.
        temp = self.spr.image
        self.spr.image = self.other_side
        self.other_side = temp
        

    def drag(self, n):
        if(n==1 and self.spr.point_on_sprite(ug.mouse_pos())):
            self.drag_flag = True
        elif(n==0):
            self.drag_flag = False


    number = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
 #card color    
    def __init__(self, color, number): 
        self.other_side = ug.card_back
        self.color = color
        img_array = []
        self.number = number
        if(self.color == Color.RED):
            img_array = ug.red_cards
        elif(self.color == Color.BLUE):
            img_array = ug.blue_cards
        elif(self.color == Color.GREEN):
            img_array = ug.green_cards
        elif(self.color == Color.YELLOW):
            img_array = ug.yellow_cards

        self.spr = ug.GameSprite(random.randrange(0,450), random.randrange(0,450), 80, 112, img_array[number],click_handler=self.drag)
        self.drag_flag=False
  

class Deck:
    def draw_handler(self,n): #Index 0
        if(n==1 and self.spr.point_on_sprite(ug.mouse_pos())):
            self.draw_flag = True

    def play_handler(self,n): #Index 1
        if(n==0 and self.spr.point_on_sprite(ug.mouse_pos())):
            print("play handler")
            for s in ug.visible_sprites:
                if(s.is_touching(self.spr)):
                    print("found card")
                    self.card_sprite_buffer = s
            self.play_flag = True

    def __init__(self, is_standard = True, is_hidden = False, handler_index = 0):
        self.draw_flag = False
        self.card_sprite_buffer = None #Holds a reference to a dropped card
        self.play_flag = False #set this to true to have the current player attempt to play the held card
        self.spr = ug.GameSprite(500,(750//2)-56,80,112,ug.card_back)
        if(handler_index == 0):
            self.spr.click_handler = self.draw_handler
        else:
            self.spr.click_handler = self.play_handler

        self.create_deck()
        self.is_hidden = is_hidden
        if(is_standard):
            self.init_deck()
            self.shuffle_deck()
        if(is_hidden or self.top_card() == None):
            print("True")
            img = ug.card_back
        else:
            img = self.top_card().spr.image
        self.spr.image = img
        self.prev_top_card = self.top_card()
        
    def create_deck(self,):
        self.cards = deque()
        
    def add_card(self,c):
        self.cards.append(c)
        if not self.is_hidden:
            self.spr.image = self.top_card().spr.image
        
    def init_deck(self,):
        for col in Color:

            self.add_card(Card(col,0))
            for i in range(9):
                self.add_card(Card(col,i+1))
            for i in range(9):
                self.add_card(Card(col,i+1))

            self.add_card(Card(col,10))
            self.add_card(Card(col,10))
            
            self.add_card(Card(col,11))
            self.add_card(Card(col,11))

            self.add_card(Card(col,12))
            self.add_card(Card(col,12))

            

        
    def is_empty(self):
        return len(self.cards)==0     

    def reset(self):
        self.cards.clear()       
    def shuffle_deck(self,):
        random.shuffle(self.cards)
        
    

    def top_card(self,):
        if(len(self.cards) == 0):
            return None
        return self.cards[-1]

    def draw_card(self,):
        
        if(self.top_card() == None):
            return None
        if(len(self.cards) == 1):
            self.spr.image = ug.card_back
        elif(not self.is_hidden):
            self.spr.image = self.top_card().spr.image
        return self.cards.pop()
    
    def is_playable(self,c):
        top = self.top_card()
        if(top == None):
            return True
        if c.color == top.color or c.number == top.number:
            return True
        return False
    

    def has_update(self): #Returns true if the top card of the deck has changed since this function was last called.
        upd = self.top_card() != self.prev_top_card
        if(upd):
            self.prev_top_card = self.top_card()
            return True
        return False

    def __str__(self):
        out = ""
        for c in self.cards:
            out = out + "\nCard: " + str(c.color) + " ,"+ str(c.number) 
        return out

        
    




    
        

    
    
        
